Changes You will Need to make in the code.

1. Update any information for your event in the copy.

2. Update the Masthead images in Photoshop to include your event details. And save as either mastheadx2-650x300.jpg or mastheadx2-320x340.jpg.

3. Change the Responsive Style Sheet to point to your new responsive masthead. -

Find the following -

img[class="masthead"] {
			content: url(http://edm.torpedogroup.com/email/ado1522/xxx-xxx-xxx/mastheadx2-320x340.jpg);
			
and change the 'url()' to match where you are hosting your images.

4. Add in links to your event page. Search the code for #YourLinkHere and replace with your event page URL.

5. Add in the view online link to your hosted page. Search the code for #ViewOnlineLink and replace with the URL for your online version.